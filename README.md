# TypeScript with formatting and test (Vitest) on NodeJS

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=typescript_node_vitest/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
nix flake new --template "gitlab:pinage404/nix-sandboxes#typescript_node_vitest" ./your_new_project_directory
```
