import { describe, expect, it } from "vitest"

// Pierre Papier Ciseaux
// Pierre vs Papier = Papier gagne
// Pierre vs Ciseaux = Pierre gagne
// Papier vs Ciseaux = Ciseaux gagne
// Papier vs Papier = Égalité

enum Geste {
  PIERRE = "pierre",
  PAPIER = "papier",
  CISEAUX = "ciseaux",
}

describe("Chifoumi", () => {
  it("pierre vs ciseaux, alors pierre gagne", () => {
    expect(chifoumi(Geste.PIERRE, Geste.CISEAUX)).toEqual("pierre")
    expect(chifoumi(Geste.CISEAUX, Geste.PIERRE)).toEqual("pierre")
  })
  it("mêmes gestes alors égalité", () => {
    expect(chifoumi(Geste.PIERRE, Geste.PIERRE)).toEqual("égalité")
    expect(chifoumi(Geste.PAPIER, Geste.PAPIER)).toEqual("égalité")
    expect(chifoumi(Geste.CISEAUX, Geste.CISEAUX)).toEqual("égalité")
  })
  it("ciseaux vs papier, alors ciseaux gagne", () => {
    expect(chifoumi(Geste.CISEAUX, Geste.PAPIER)).toEqual("ciseaux")
    expect(chifoumi(Geste.PAPIER, Geste.CISEAUX)).toEqual("ciseaux")
  })

  it("pierre vs papier, alors papier gagne", () => {
    expect(chifoumi(Geste.PIERRE, Geste.PAPIER)).toEqual("papier")
    expect(chifoumi(Geste.PAPIER, Geste.PIERRE)).toEqual("papier")
  })
})

function geste2Gagne(geste1: Geste, geste2: Geste): boolean {
  return (
    (geste1 == Geste.PIERRE && geste2 == Geste.PAPIER) ||
    (geste1 == Geste.PAPIER && geste2 == Geste.CISEAUX) ||
    (geste1 == Geste.CISEAUX && geste2 == Geste.PIERRE)
  )
}

function chifoumi(geste1: Geste, geste2: Geste): string {
  if (geste1 == geste2) {
    return "égalité"
  }

  return geste2Gagne(geste1, geste2) ? geste2 : geste1
}
